[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![Python 3.11](https://img.shields.io/badge/python-3.11-green.svg)](https://www.python.org/downloads/release/python-3110/)
[![License: GPL v3+](https://img.shields.io/badge/License-GPLv3+-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![black]( https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
![Version 0.0.1](https://img.shields.io/badge/version-0.0.1-blue.svg)

# OPCUA Dummy Server.
Pending.

### Table of contents
- [Introduction](#introduction)
- [How to use this device](#how-to-use-this-device)
- [Device Properties](#device-properties)
- [Device Commands](#device-commands)
- [Configuration files](#configuration-files)
- [Related proyects](#related-proyects)
- [About](#about)


# -*- coding:utf-8 -*-
# ##### BEGIN GPL LICENSE BLOCK #####
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####


from setuptools import setup, find_packages

description = "OPCUA Dummy Tango device server."
long_description = (
    "Project to test Tango OPCUA communication."
    "The idea is to provide an interface to test new developments."
)

author = "Emilio Morales"
maintainer = author
maintainer_email = "emorales@cells.es"
__version__ = "0.0.1"
url = "https://gitlab.com/opcua-test-and-developments/opcuadummyserver"
download_url = url
copyright = "Copyright 2023, CELLS / ALBA Synchrotron"
platforms = ["Linux"]


install_requires = ["pytango"]

entry_points = {
    "console_scripts": ["OPCUADummyDS = src.OPCUADummy:main"],
}

classifiers = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    (
        "License :: OSI Approved :: "
        + "GNU General Public License v3 or later (GPLv3+)"
    ),
    "Operating System :: POSIX :: Linux",
    "Operating System :: Unix",
    "Operating System :: OS Independent",
    "Natural Language :: English",
    "Programming Language :: Python :: 3.10",
    "Programming Language :: Python :: 3.11",
    "Topic :: Scientific/Engineering",
]
setup(
    name="OPCUADummyDevice",
    version=__version__,
    description=description,
    long_description=long_description,
    author=author,
    maintainer=maintainer,
    maintainer_email=maintainer_email,
    url=url,
    download_url=download_url,
    platforms=platforms,
    license="GPLv3+",
    packages=find_packages(),
    classifiers=classifiers,
    include_package_data=True,
    entry_points=entry_points,
    python_requires=">=3.7",
    install_requires=install_requires,
)
